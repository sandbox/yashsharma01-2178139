MODULE
------
Profile URL


CREDITS
-------
Developed by Yash Sharma <yashsharma01 at gmail dot com>


REQUIREMENTS
------------
 # Drupal 7.0


DESCRIPTION/FEATURES
--------------------
  This module allows user to create public profile url.


FEATURES
---------
 # Create public profile url.


INSTALLATION
------------
 # Decompress the profile_url.tar.gz file into your Drupal modules
   directory (usually sites/all/modules).
 # Enable the Profile URL : Administration > Modules (admin/modules).
 # User can see link "Profile URL" under My Account tab.
